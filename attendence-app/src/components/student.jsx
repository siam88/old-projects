import React, { Component } from "react";

export class Student extends Component {
  state = {
    present: this.props.present,
    absent: this.props.absent
  };
  formatPresentCount = () => {
    const { present } = this.state;
    return present === 0 ? "zero" : present;
  };
  formatAbsentCount = () => {
    const { absent } = this.state;
    return absent === 0 ? "zero" : absent;
  };
  formatBadgePresent = () => {
    let classes = "badge m-2 badge-";
    classes += this.state.present === 0 ? "warning" : "secondary";
    return classes;
  };
  formatBadgeAbsent = () => {
    let classes = "badge m-2 badge-";
    classes += this.state.absent === 0 ? "warning" : "secondary";
    return classes;
  };
  handlePresent = () => {
    return this.setState({ present: this.state.present + 1 });
  };
  handleAbsent = () => {
    return this.setState({ absent: this.state.absent + 1 });
  };
  handleReset = () => {
    return this.setState({
      present: 0,
      absent: 0
    });
  };
  render() {
    return (
      <div>
        {this.props.children}
        <button
          onClick={this.handlePresent}
          className="waves-effect waves-light btn-small m-2 "
        >
          present
        </button>
        <span className={this.formatBadgePresent()}>
          Present:{this.formatPresentCount()}
        </span>

        <span className={this.formatBadgeAbsent()}>
          Absent:{this.formatAbsentCount()}
        </span>
        <button
          onClick={this.handleAbsent}
          className="waves-effect waves-light btn-small m-2"
        >
          Absent
        </button>
      </div>
    );
  }
}

export default Student;
