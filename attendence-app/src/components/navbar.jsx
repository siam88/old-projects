import React, { Component } from "react";

export class Navbar extends Component {
  render() {
    return (
      <div>
        <nav class="nav-extended">
          <div class="nav-wrapper">
            <a href="#!" class="brand-logo">
              SEU
            </a>
            <ul class="right hide-on-med-and-down">
              <li>
                <a>A link</a>
              </li>
              <li>
                <a>A second link</a>
              </li>
              <li>
                <a>A third link</a>
              </li>
            </ul>
          </div>
          <div class="nav-content">
            <span class="nav-title">Student Attendence</span>
            <a class="btn-floating btn-secondary btn-large halfway-fab waves-effect waves-light teal">
              <i class="material-icons">add</i>
            </a>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
