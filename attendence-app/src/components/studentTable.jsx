import React, { Component } from "react";
import Student from "./student";
export class StudentTable extends Component {
  state = {
    students: [
      { id: 1, present: 0, absent: 0, name: "siam" },
      { id: 2, present: 0, absent: 0, name: "shipa" },
      { id: 3, present: 0, absent: 0, name: "nafiul" },
      { id: 4, present: 0, absent: 0, name: "vuttu" }
    ]
  };
  handleDelete = () => {
    console.log("delete click");
    // const deletes = this.state.students.filter(e => e.id !== studentId);
    // this.setState({ students: deletes });
  };
  render() {
    return (
      <div className="row">
        <div className="col s4 offset-s4">
          {this.state.students.map(e => (
            <Student
              key={Student.id}
              present={e.present}
              absent={e.absent}
              onDelete={this.handleDelete}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default StudentTable;
