import React, { Component } from "react";
import "./App.css";
import Navbar from "./components/navbar";
import StudentTable from "./components/studentTable";
import Student from "./components/student";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Navbar />
        <StudentTable />
      </React.Fragment>
    );
  }
}

export default App;
