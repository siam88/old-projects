import React, { Component } from "react";

export class Counter extends Component {
  state = {
    value: this.props.counter.value,
    imgUrl: "https://picsum.photos/400",
    tegs: ["teg1", "teg2", "teg3"]
  };

  formatCount = () => {
    const { value: value } = this.props.counter;
    return value === 0 ? <h4>ZERO</h4> : value;
  };

  changeBadges = () => {
    let badges = "badge m-2 badge-";
    badges += this.props.counter.value === 0 ? "warning" : "primary";
    return badges;
  };

  style = { fontWeight: "bold", fontSize: 15 };
  render() {
    return (
      <div>
        <span style={this.style} className={this.changeBadges()}>
          {this.formatCount()}
        </span>
        <button
          className="btn btn-secondary btn-lg"
          onClick={() => this.props.onIncrement(this.props.counter)}
        >
          INCREMENT
        </button>
        <button
          className="btn btn-secondary btn-lg m-2"
          onClick={() => this.props.onDicrement(this.props.counter)}
        >
          DICREMENT
        </button>
        <button
          className="btn btn-danger btn-lg m-2"
          onClick={() => this.props.onDelete(this.props.counter.id)}
        >
          DELETE
        </button>
      </div>
    );
  }
}

export default Counter;
