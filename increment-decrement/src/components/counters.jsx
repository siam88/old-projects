import React, { Component } from "react";
import Counter from "./counter";

export class Counters extends Component {
  render() {
    return (
      <div>
        <button className="btn btn-primary btn-lg" onClick={this.props.onRest}>
          RESET
        </button>
        {this.props.counters.map(e => (
          <Counter
            key={e.id}
            onDelete={this.props.onDelete}
            counter={e}
            onIncrement={this.props.onIncrement}
            onDicrement={this.props.onDicrement}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
