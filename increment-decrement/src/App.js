import React, { Component } from "react";
import "./App.css";
import { Counter } from "./components/counter";
import { Counters } from "./components/counters";
import Navbar from "./components/navbar";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 0 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]
  };
  handleDelete = counterid => {
    const counters = this.state.counters.filter(e => e.id !== counterid);
    this.setState({ counters: counters });
  };
  handleReset = () => {
    const counters = this.state.counters.map(e => {
      e.value = 0;
      return e;
    });
    this.setState({ counters: counters });
  };
  handleIncrement = counter => {
    const counters = [...this.state.counters];
    const index = this.state.counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({
      counters: counters
    });
  };
  handleDiccrement = counter => {
    const counters = [...this.state.counters];
    const index = this.state.counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value--;
    this.setState({
      counters: counters
    });
  };
  render() {
    return (
      <React.Fragment>
        <Navbar
          totalCount={this.state.counters.filter(e => e.value > 0).length}
        />
        <main className="container">
          <Counters
            onRest={this.handleReset}
            onIncrement={this.handleIncrement}
            onDicrement={this.handleDiccrement}
            onDelete={this.handleDelete}
            counters={this.state.counters}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
