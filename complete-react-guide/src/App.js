import React, { Component } from 'react';
import Person from './Person/Person';
import './App.css';
import Radium ,{StyleRoot} from 'radium'

class App extends Component {
  state = {
    persons: [
      { id: 'abc1', name: 'MAX', age: 28 },
      { id: 'abc111', name: 'MANU', age: 29 },
      { id: 'abc11111', name: 'STAPHINI', age: 17 }
    ],
    togglePersonHandler: false
  };

  togglePersonHandler = () => {
    const currentPerson = this.state.togglePersonHandler;
    this.setState({
      togglePersonHandler: !currentPerson
    });
  };
  deletePersonHandler = e => {
    const persons = [...this.state.persons];
    persons.splice(e, 1);
    this.setState({ persons });
  };
  nameChangeHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = {
      ...this.state.persons[personIndex]
    };
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState({
      persons
    });
  };
  render() {
    const style = {
      backgroundColor: 'green',
      color: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      ":hover":{
        backgroundColor:'lightgreen',
        color:"black"
      }
    };

    let person = null;
    if (this.state.togglePersonHandler) {
      person = (
        <div>
          {this.state.persons.map((person, index) => {
            return (
              <Person
                name={person.name}
                age={person.age}
                key={person.id}
                click={() => this.deletePersonHandler(index)}
                changed={event => this.nameChangeHandler(event, person.id)}
              />
            );
          })}
        </div>
      );
      style.backgroundColor = 'red';
      style[':hover']={
        backgroundColor: 'salmon ',
        color: "black"
      }
    }
    const classes = [];
    if (this.state.persons.length <= 2) {
      classes.push('red');
    }
    if (this.state.persons.length <= 1) {
      classes.push('bold');
    }

    return (
      <div className='App'>
        <h1> Hi, I am a react app </h1>
        <p className={classes.join('  ')}> This is really working </p>{' '}
        <button style={style} onClick={this.togglePersonHandler}>
          switch body
        </button>
        {person}
      </div>
    );
    // return React.createElement("div",{classname:"App"},React.createElement("h1",null,"hi I am react app"));
  }
}

export default Radium(App);
