import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './component/header';
import Items from "./component/items";
import AddTodo from './component/addTodo';


class App extends Component {
state={
  item:{
    title:"",date:""
    },
  items: [
    { key: 1,title: "programming", date: "2018-11-12" },
    { key: 2,title: "programming-2", date: "2018-12-12" },
    { key: 3, title: "programming-3", date: "2018-08-12" }
  ]
};
  handleDelete(itemTobeDeleted){
      var newItems=this.state.items.filter((_item) =>{
        return _item !=itemTobeDeleted
      });
      this.setState({
        items:newItems
      });

  }
HandleAddItems=() => {
    var items=this.state.items;
    console.log(items);
  const item = { ...this.state.item };
    items.push(item);
    this.setState(
      {
        items:items
      }
    );
    
};

handleOnChangeTitle= e => {
    var v =e.target.value;
    var item = this.state.item;
    item.title = v;
    this.setState(
      {
        item: item
      }
  );
  };
  handleOnChangeDate= e =>{
    var d=e.target.value;
    var item = this.state.item;
    item.date = d;
    this.setState(
      {
        item: item
      }
    );
  }

  render() {
    return (
      <div className="container">
        <Header />
          <div className="row">
            <div className="col s6">
            <AddTodo HandleAddItems={this.HandleAddItems} 
            handleOnChangeTitle={this.handleOnChangeTitle}
              handleOnChangeDate={this.handleOnChangeDate}/>
            </div>
            <div className="col s6">
            <Items items={this.state.items} 
              handleDelete={this.handleDelete.bind(this)}/>
            </div>
          </div>
  
      </div>
    );
  }
}

export default App;
