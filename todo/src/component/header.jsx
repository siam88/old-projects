import React, { Component } from 'react';

class Header extends Component{
    render(){
        return(
            <div>
            
                <nav>
                    <div className="nav-wrapper">
                        <a href="#" className="brand-logo center">TODO APPs assigned by rabi vai</a>
                        <ul id="nav-mobile" className="left hide-on-med-and-down">                            
                        </ul>
                    </div>
                </nav>
            </div>
        );
    }
} 
export default Header;