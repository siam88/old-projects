import React, { Component } from 'react';

class AddTodo extends Component{
    render(){
        return(
            <div  >    
 
                <label htmlFor="title"/>
                <input type="text" id="title" onChange={e => this.props.handleOnChangeTitle(e)} />

                <label htmlFor="Date"/>
                <input type="date" id="date" className="datepicker" onChange={e => this.props.handleOnChangeDate(e)} />

               
                
                <button class="btn waves-effect waves-light" type="submit" name="action" onClick={e => this.props.HandleAddItems()}>Submit
                    <i class="material-icons right">send</i>
                </button>
            </div>    
        );
    }
}

export default AddTodo;