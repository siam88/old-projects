import React, { Component } from 'react';

class Items extends Component{
 

    render(){
        return(
            <div >               
                <ul className="collection">
                
                        {this.props.items.map(e =>(
                        <li className="collection-item">
                            {e.date}--->{e.title}
                            <button class="waves-effect waves-light btn right" onClick={this.props.handleDelete.bind(null,e)}> X</button>
                        </li>
                        ))}
                </ul>
                   
                
            </div>
        );
    }
}

export default Items;