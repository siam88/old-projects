function add(a, b) {
    return a + b;
}

function sub(a, b) {
    return a - b;
}
const obj = {
    name: "jon doe"
}

// module.exports.add=add;
// module.exports.sub=sub;
// module.exports.obj=obj;

module.exports = {
    add,
    sub,
    obj
}