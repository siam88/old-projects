const express=require('express');
const bodyParser=require('body-parser')
const app=express();

app.use(bodyParser.json());

var persons=[
    {id:1,name:"hasibur"},
    {id:2,name:"Rafsan"},
    {id:3,name:"hedaetun"}
];

app.get("/persons",(req,res)=>{
    return res.send(persons);
});

app.get("/person/:id",(req,res)=>{
    const person=persons.find(e=>e.id==req.params.id);
    return res.send(person);
});

app.post("/persons",(req,res)=>{
    var persons=[];
    false.readFile("./persons")
    persons.push(JSON.parse(req.body));
    return res.send(JSON.parse(req.body));
});

app.put("/person/:id",(req,res)=>{
    const person=persons.find(e=>e.id==req.params.id);
    if(person){
        persons=persons.map(p=>{
            if(p.id==req.params.id){
                p=req.body;
               
            }
            return p;
        });
        return res.send(person);
    }else{
        return res.status(404).send("No person with this id")
    }
})
app.listen(3000,()=>console.log("listening port 3000 ..."))