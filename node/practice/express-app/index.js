const express = require("express");
const bodyParser = require("body-parser");
const fs = require("fs");

const app = express();
app.use(bodyParser.json());

let persons = [
  { id: 1, name: "Siam" },
  { id: 2, name: "Rafsan" },
  { id: 3, name: "hoque" }
];

app.get("/persons", (request, response) => {
  return response.send(persons);
});

app.get("/person/:id", (request, response) => {
  const person = persons.find(e => e.id == request.params.id);
  return response.send(person);
});

app.post("/persons", (request, response) => {
  var persons = [];
  fs.readFile("./persons.txt", { encoding: "utf-8" }, (err, result) => {
    if (err) {
      console.log(err);
      throw new Error();
    } else {
      persons = JSON.parse(result);
      persons.push(request.body);
      fs.writeFile("./persons.txt", JSON.stringify(persons), err =>
        console.log(err)
      );
    }
  });

  return response.send(request.body);
});

app.put("/person/:id", (request, response) => {
  const person = persons.find(e => e.id == request.params.id);
  if (person) {
    persons = persons.map(e => {
      if (e.id == request.params.id) {
        e = request.body;
      }
      return e;
    });
    return response.send(request.body.person);
  } else {
    return response.status(404).send("no person with this id    ");
  }
});

app.listen(3000, () => console.log("listening to the port 3000 ...."));
