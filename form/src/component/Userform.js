import React, { Component } from 'react';
import Formuserdetails from './Formuserdetails';
import Formpersonaldetails from './Formpersonaldetails';
import Confirm from './Confirm';
import Success from './Success';

export class Userform extends Component {
state={
    step:1,
    firstname:'',
    lastname:'',
    email:'',
    itemName:'',
    itemType:'',
    restaurantName:'',
    location:'',
    price:'',
}


//procced to next step
nextstep=()=>{
    const {step}=this.state;
    this.setState(
        {
            step:step+1
        }
    );
}
//procced to previous step
prestep=()=>{
    const{step}=this.state;
    this.setState(
        {
            step:step-1
        }
    );
}
//procced to the first step
goFirst=()=>{
    const{step}=this.state;
    this.setState(
        {
            step:1
        }
    );
}
//handlefield change
handlechange= input =>e=>{
    this.setState({[input]:e.target.value});
}


    render() {
        const{step}=this.state;
        const{firstname,lastname,email,itemName,itemType,restaurantName,location,price}=this.state;
        const values={firstname,lastname,email,itemName,itemType,restaurantName,location,price}

        switch(step){
            case 1:
                return(
                    <Formuserdetails
                        nextstep={this.nextstep}
                        handlechange={this.handlechange}
                        values={values}
                    />
                )
            case 2:
                return (
                    <Formpersonaldetails
                        nextstep={this.nextstep}
                        prestep={this.prestep}
                        handlechange={this.handlechange}
                        values={values}
                    />
                )
            case 3:
                return (
                    <Confirm
                        nextstep={this.nextstep}
                        prestep={this.prestep}
                        values={values}
                    />
                )
            case 4:
                return <Success
                        goFirst={this.goFirst}
                />
            
        }
  }
}

export default Userform
