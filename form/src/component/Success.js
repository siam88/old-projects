import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';

import RaisedButton from 'material-ui/RaisedButton';
export class Success extends Component {
    backFirst = e => {
        e.preventDefault();
        this.props.goFirst();
    }
    render() {
        const styles = {
            button: {
                margin: 15
            }
        }
        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <AppBar title="Confirm User Data" />
                        <h1>Thanks for giving your valuable review</h1>                        
                    <br />
                    <RaisedButton label="Back" primary={true} style={styles.button} onClick={this.backFirst} />

                </React.Fragment>
            </MuiThemeProvider>
        )
    }


}

export default Success
