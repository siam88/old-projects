import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
export class Formuserdetails extends Component {
    continue =e =>{
        e.preventDefault();
        this.props.nextstep();
    }
  render() {
      const { values,handlechange}=this.props;
      const styles = {
        button : {
            margin:15
        }
      }
    return (
      <MuiThemeProvider>
            <React.Fragment>
                <AppBar title="Reviewer info"/>
                
                <TextField hintText="Enter your First Name" floatingLabelText="First Name" onChange={handlechange('firstname')} defaultValue={values.firstname} />
                <br/>

                <TextField hintText="Enter your Last Name" floatingLabelText="Last Name" onChange={handlechange('lastname')} defaultValue={values.lastname} />
                <br />

                <TextField hintText="Enter your Email" floatingLabelText="Email" onChange={handlechange('email')} defaultValue={values.email} />
                <br />

                <RaisedButton label="continue" primary={true} style={styles.button} onClick={this.continue} />
                    
            </React.Fragment>
      </MuiThemeProvider>
    )
  }

  
}

export default Formuserdetails
