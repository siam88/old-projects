import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import {List,ListItem} from 'material-ui/List';
import RaisedButton from 'material-ui/RaisedButton';
export class Confirm extends Component {
    continue = e => {
        e.preventDefault();
        
        //process form//

        this.props.nextstep();
    }
    back = e => {
        e.preventDefault();
        this.props.prestep();
    }
    render() {
        const { values: { firstname, lastname, email, itemName, itemType, restaurantName, location, price} } = this.props;
        const val = { firstname, lastname, email, itemName, itemType, restaurantName, location, price}
        
        const styles = {
            button: {
                margin: 15
            }
        }
        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <AppBar title="Confirm User Data" />
                        <List>
                         <ListItem primaryText="first Name" secondaryText={firstname}/>
                        <ListItem primaryText="last Name" secondaryText={lastname} />
                        <ListItem primaryText="Email" secondaryText={email} />
                        <ListItem primaryText="itemName" secondaryText={itemName} />
                        <ListItem primaryText="itemType" secondaryText={itemType} />
                        <ListItem primaryText="restaurantName" secondaryText={restaurantName} />
                        <ListItem primaryText="location" secondaryText={location} />
                        <ListItem primaryText="price" secondaryText={price} />
                       

                        </List>
                    <br />

                    <RaisedButton label="Confirm & Continue" primary={true} style={styles.button} onClick={this.continue} />
                    <RaisedButton label="Back" primary={false} style={styles.button} onClick={this.back} />

                </React.Fragment>
            </MuiThemeProvider>
        )
    }


}

export default Confirm
