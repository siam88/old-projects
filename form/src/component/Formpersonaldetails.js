import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
export class Formpersonaldetails extends Component {
    continue = e => {
        e.preventDefault();
        this.props.nextstep();
    }
    back = e => {
        e.preventDefault();
        this.props.prestep();
    }
    render() {
        const { values, handlechange } = this.props;
        const styles = {
            button: {
                margin: 15
            }
        }
        return (
            <MuiThemeProvider>
                <React.Fragment>
                    <AppBar title="Enter food and restaurant details" />

                    <TextField hintText="Enter your Item Name" floatingLabelText="Item Name" onChange={handlechange('itemName')} defaultValue={values.itemName} />
                    <br />

                    <TextField hintText="Enter your itemType" floatingLabelText="Item Type" onChange={handlechange('itemType')} defaultValue={values.itemType} />
                    <br />

                    <TextField hintText="Enter your Restaurant Name" floatingLabelText="Restaurant Name" onChange={handlechange('restaurantName')} defaultValue={values.restaurantName} />
                    <br />

                    <TextField hintText="Enter your Location" floatingLabelText="Location" onChange={handlechange('location')} defaultValue={values.location} />
                    <br />

                    <TextField hintText="Enter your Price" floatingLabelText="Price" onChange={handlechange('price')} defaultValue={values.price} />
                    <br />

                    <RaisedButton label="Continue" primary={true} style={styles.button} onClick={this.continue} />
                    <RaisedButton label="Back" primary={false} style={styles.button} onClick={this.back} />

                </React.Fragment>
            </MuiThemeProvider>
        )
    }


}

export default Formpersonaldetails
