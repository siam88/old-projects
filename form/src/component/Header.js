import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Drawer from 'material-ui/Drawer';

export class Header extends Component{
    constructor(){
        super();
        this.state={
            drawerOpened:false
        }
    };

    _toggleDrawer () {
        this.setState=({
            drawerOpened:!this.state.drawerOpened
        });
    };
    render(){
        return(
            <MuiThemeProvider>
                <React.Fragment>
                <div>
                    <AppBar title="FOOD BOOK " onLeftIconButtonTouchTap={()=>this.toggleDrawer()} />
                    <Drawer open={this.state.drawerOpened} docked={false} onRequestChange={()=>this.toggleDrawer()}/>
                </div>
                
                </React.Fragment>
            </MuiThemeProvider>
        )
    }
}

export default Header