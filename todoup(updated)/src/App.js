import React, { Component } from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Todos from "./components/todos";
import Header from "./components/header";
import AddTodo from "./components/addTodo";
import uuid from "uuid";
import About from "./components/about";
class App extends Component {
  state = {
    todos: [
      { id: uuid.v4(), title: "wash ur cloth", complete: false },
      { id: uuid.v4(), title: "wash ur pant", complete: false },
      { id: uuid.v4(), title: "wash ur PAJAMa", complete: false }
    ],
    todo: [{ id: null, title: "", complete: false }]
  };
  markCompleted = id => {
    var todos = [...this.state.todos];
    todos.map(e => {
      if (e.id === id) {
        e.complete = !e.complete;
      }
    });
    this.setState({ todos });
  };
  delTodo = id => {
    this.setState({
      todos: [...this.state.todos.filter(e => e.id !== id)]
    });
  };
  addTodo = e => {
    const newItem = { id: uuid.v4(), title: e, complete: false };
    this.setState({
      todos: [...this.state.todos, newItem]
    });
  };
  render() {
    return (
      <Router>
        <div>
          <Header />
          <Route
            exact
            path="/"
            render={props => (
              <React.Fragment>
                <AddTodo addTodo={this.addTodo} />
                <Todos
                  todos={this.state.todos}
                  markCompleted={this.markCompleted}
                  delTodo={this.delTodo}
                />
              </React.Fragment>
            )}
          />
          <Route
            path="/about"
            render={props => (
              <React.Fragment>
                <About />
              </React.Fragment>
            )}
          />
        </div>
      </Router>
    );
  }
}

export default App;
