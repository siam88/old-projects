import React, { Component } from "react";
import TodoItem from "./todoItem";

export class Todos extends Component {
  render() {
    return this.props.todos.map(e => (
      <TodoItem
        key={e.id}
        todo={e}
        markCompleted={this.props.markCompleted}
        delTodo={this.props.delTodo}
      />
    ));
  }
}

export default Todos;
