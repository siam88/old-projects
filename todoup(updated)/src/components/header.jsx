import React, { Component } from "react";
import { Link } from "react-router-dom";
export class Header extends Component {
  headerStyle = () => {
    return {
      background: "#333",
      color: "White",
      textAlign: "center",
      padding: "40px"
    };
  };

  render() {
    return (
      <div style={this.headerStyle()}>
        <h1>T O D O A P P</h1>
        <Link className="link" to="/">
          Home
        </Link>{" "}
        |{" "}
        <Link className="link" to="/about">
          About
        </Link>
      </div>
    );
  }
}

export default Header;
