import React, { Component } from "react";

export class TodoItem extends Component {
  itemStyle = () => {
    return {
      textDecoration: this.props.todo.complete ? "line-through" : "none"
    };
  };
  btnStyle = () => {
    return {
      background: "#f4f4f4",
      float: "right",
      marginBottom: "1px",
      padding: "1px 15px",
      display: "inline-block",
      boxShadow: "1px 1px 3px black "
    };
  };
  render() {
    const { id, title } = this.props.todo;
    return (
      <div style={this.itemStyle()} className="itembtn">
        <p>
          <input
            type="checkbox"
            onChange={this.props.markCompleted.bind(this, id)}
          />{" "}
          {title}{" "}
          <button
            onClick={this.props.delTodo.bind(this, id)}
            className="btnstyle"
          >
            x
          </button>
        </p>
      </div>
    );
  }
}

export default TodoItem;
