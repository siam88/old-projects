

import React from 'react';
import logo from './artboard-siam-name2.png';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      newItem: '',
      list: []
    };
  }

  additem(todoValue) {
    if (todoValue !== '') {
      const newItem = {
        id: Date.now,
        value: todoValue,
        isDone: false
      };
      const list = [...this.state.list];
      list.push(newItem);

      this.setState({
        list,
        newItem: ''
      });
    }
  }

  deleteitem(id) {
    const list = [...this.state.list];
    const updatelist = list.filter(item => item.id !== id);
    this.setState({
      list: updatelist
    });
  }

  updateitem(input) {
    this.setState({
      newItem: input
    });
  }

  render() {
    return (
      <div className='App'>
        <header className='App-header'>
          <img
            src={logo}
            className='App-logo'
            width='18%'
            height='100px'
            alt='logo'
          />
          <p>learn code with siam</p>
        </header>

        <div style={{ color: 'red' }}>
          <input
            type='text'
            className='input-text'
            placeholder='write a todo'
            required
            value={this.state.newItem}
            onChange={e => this.updateitem(e.target.value)}
          />
          <button
            className='add-btn'
            onClick={() => this.additem(this.state.newItem)}
            disabled={!this.state.newItem.length}
          >
            Add todo
          </button>
          <div className='list'>
            <ul>
              {this.state.list.map(item => {
                return (
                  <li key={item.id}>
                    <input
                      type='checkbox'
                      name='isDone'
                      checked={item.isDone}
                      onChange={() => {}}
                    />
                    {item.value}
                    <button
                      className='delete'
                      onClick={() => this.deleteitem(item.id)}
                    >
                      Delete
                    </button>
                  </li>
                );
              })}
              <li>
                <input type='checkbox' name='' id='' />
                complete exam
                <button className='btn'>Delete</button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
export default App;
