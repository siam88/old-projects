import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navbar from "./components/Navbar";
import Heading from "./components/Heading";
import Form from "./components/Form";
import Search from "./components/Search";
import Datatable from "./components/Datatable";
import { About } from "./components/About";
import Info from "./components/Info";
import Footer from "./components/Footer";
import "./App.css";

class App extends Component {
  changeRate = count => {};

  state = {
    items: [
      {
        id: 3,
        author: "Shipa",
        name: "RiceBowl",

        restrurant: "pabna",

        price: "80",
        rating: 2
      },
      {
        id: 4,
        author: "Shipa",
        name: "Burger",

        restrurant: "chillox",

        price: "240",
        rating: 3
      },
      {
        id: 5,
        author: "Shipa",
        name: "Pizza",

        restrurant: "TakeOut",

        price: "500",
        rating: 1
      }
    ],

    item: {
      id: null,
      author: "",
      name: "",

      restrurant: "",

      price: "",
      rating: ""
    }
  };

  authorName = e => {
    var item = { ...this.state.item };
    item.author = e.target.value;
    this.setState({ item });
  };

  foodName = e => {
    var item = { ...this.state.item };
    item.name = e.target.value;
    this.setState({ item });
  };

  inputRestrurant = e => {
    var item = { ...this.state.item };
    item.restrurant = e.target.value;
    this.setState({ item });
  };

  inputPrice = e => {
    var item = { ...this.state.item };
    item.price = e.target.value;
    this.setState({ item });
  };
  inputRate = e => {
    var item = { ...this.state.item };
    item.rating = e.target.value;
    this.setState({ item });
  };
  addItem = () => {
    var items = [...this.state.items];
    items.push(this.state.item);
    this.setState({ items });
  };

  deleteItem = id => {
    console.log(id);
    this.setState({
      items: [...this.state.items.filter(e => e.id !== id)]
    });
  };
  searchAction = fName => {
    console.log(fName);
  };

  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <Route
            exact
            path="/"
            render={props => (
              <React.Fragment>
                <Form
                  item={this.props.item}
                  authorName={this.authorName}
                  foodName={this.foodName}
                  inputRestrurant={this.inputRestrurant}
                  inputPrice={this.inputPrice}
                  inputRate={this.inputRate}
                  addItem={this.addItem}
                />
                <Search searchAction={this.searchAction} />
                <Datatable
                  items={this.state.items}
                  deleteItem={this.deleteItem}
                />
              </React.Fragment>
            )}
          />
          <Route
            path="/about"
            render={props => (
              <React.Fragment>
                <About />
              </React.Fragment>
            )}
          />
          <Route
            path="/info"
            render={props => (
              <React.Fragment>
                <Info />
              </React.Fragment>
            )}
          />

          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
