import React, { Component } from "react";
import Rating from "./Rating";
import Search from "./Search";
import Deleting from "./Deleting";

class Datatable extends Component {
  changeRate = count => {};

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="chart">
              <table className="highlight">
                <thead>
                  <tr>
                    <th>Author Name</th>
                    <th>Food Name</th>
                    <th>Restrurant Name</th>
                    <th>Item Price</th>
                    <th>Rating</th>
                    <th>Delete</th>
                  </tr>
                </thead>

                <tbody>
                  {this.props.items.map(e => (
                    <tr>
                      <td>{e.author}</td>
                      <td>{e.name}</td>

                      <td>{e.restrurant}</td>

                      <td>{e.price}</td>
                      <td>
                        <Rating changeRate={e.rating} />
                      </td>
                      <td>
                        <Deleting
                          itemId={e.id}
                          deleteItem={this.props.deleteItem}
                        />
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Datatable;
