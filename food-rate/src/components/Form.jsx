import React, { Component } from "react";

class Form extends Component {
  clearAll = () => {
    document.getElementById("authorName").value = "";
    document.getElementById("foodName").value = "";
    document.getElementById("inputRestrurant").value = "";
    document.getElementById("inputPrice").value = "";
    document.getElementById("inputRate").value = "";
  };
  render() {
    return (
      <div className="row">
        <div className="valign-wrapper row login-box">
          <div className=" col card hoverable s10 pull-s1 m6 pull-m3 l4 pull-l4 mainForm">
            <div className="input-field col s10 ">
              <input
                id="authorName"
                type="text"
                className="validate"
                onChange={e => this.props.authorName(e)}
              />
              <label for="last_name">Author</label>
            </div>

            <div className="input-field col s10">
              <input
                id="foodName"
                type="text"
                className="validate"
                onChange={e => this.props.foodName(e)}
              />
              <label for="last_name">Food Name</label>
            </div>

            <div className="input-field col s10">
              <input
                id="inputRestrurant"
                type="text"
                className="validate"
                onChange={e => this.props.inputRestrurant(e)}
              />
              <label for="last_name">Restrurant Name</label>
            </div>

            <div className="input-field col s10">
              <input
                id="inputPrice"
                type="number"
                className="validate"
                onChange={e => this.props.inputPrice(e)}
              />
              <label htmlFor="last_name">Price</label>
            </div>
            <div className="input-field col s10">
              <input
                id="inputRate"
                type="number"
                className="validate"
                onChange={e => this.props.inputRate(e)}
              />
              <label for="last_name">Rating</label>
            </div>

            <div class="input-field col s6">
              <button
                class="btn-floating btn-small waves-effect waves-light red"
                onClick={this.props.addItem}
              >
                <i class="material-icons">add</i>
              </button>
              <button
                className="btn-floating btn-small waves-effect waves-light red"
                onClick={this.props.addItem}
              >
                <i className="material-icons" onClick={e => this.clearAll(e)}>
                  clear_all
                </i>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Form;
