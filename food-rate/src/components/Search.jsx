import React, { Component } from "react";

export class Search extends Component {
  render() {
    return (
      <div>
        <div className="container yyyy">
          <div className="row">
            <div className="input-field col s6">
              <i className="material-icons prefix">search</i>
              <textarea
                id="icon_prefix2"
                className="materialize-textarea"
                onChange={e => this.props.searchAction(e.target.value)}
              />
              <label for="icon_prefix2">Search By Food Name</label>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Search;
