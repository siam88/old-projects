import React, { Component } from "react";

class Info extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col s12">
            <p>
              <i>
                {" "}
                Eat the right amount of calories for how active you are, so that
                you balance the energy you consume with the energy you use. If
                you eat or drink too much, you'll put on weight. If you eat and
                drink too little, you'll lose weight. Eat a wide range of foods
                to ensure that you're getting a balanced diet and that your body
                is receiving all the nutrients it needs. It is recommended that
                men have around 2,500 calories a day (10,500 kilojoules). Women
                should have around 2,000 calories a day (8,400 kilojoules). Most
                adults are eating more calories than they need, and should eat
                fewer calories.
              </i>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Info;
