import React, { Component } from "react";
import { NavLink } from "react-router-dom";

export class Navbar extends Component {
  render() {
    return (
      <div>
        <nav>
          <div class="nav-wrapper">
            <NavLink to="/" class="brand-logo">
              Food Review
            </NavLink>

            <ul id="nav-mobile" class="right hide-on-med-and-down">
              <li>
                <NavLink to="/">Drop Opinion</NavLink>{" "}
              </li>
              <li>
                <NavLink to="/about">Contact</NavLink>{" "}
              </li>
              <li>
                <NavLink to="/info">Food Tips</NavLink>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
