import React, { Component } from "react";

export class About extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col s12" />
          <h5>
            <i className="material-icons">call</i>
            01615011478
            <br />
            <i className="material-icons">email</i>abcd@gmail.com
          </h5>
        </div>
      </div>
    );
  }
}

export default About;
