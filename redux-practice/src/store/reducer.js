import * as actionTypes from "./actionTypes";

const initiaState = {
    product: 0,
    abc: 10
};

const productReducer = (state = initiaState, action) => {
    switch (action.type) {
        case actionTypes.INCREASE:
            state = {
                ...state,
                product: state.product + 1
            };
            break;
        case actionTypes.DECREASE:
            state = {
                ...state,
                product: state.product - 1
            };
            break;

        default:
            return state;
    }

    return state;
};
export default productReducer;
