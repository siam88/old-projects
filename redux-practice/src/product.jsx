import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import * as actionTypes from "./store/actionTypes";

export default function () {
    const product = useSelector(state => state.productReducer.product);
    const abc = useSelector(state => state.productReducer.abc);
    const dispatch = useDispatch();

    return (
        <div>
            <p>{product}</p>
            <button onClick={() => dispatch({ type: actionTypes.INCREASE })}>
                Increase
      </button>
            <button onClick={() => dispatch({ type: actionTypes.DECREASE })}>
                decrease
      </button>

        </div >
    );
}
